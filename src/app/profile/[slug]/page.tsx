"use client";
import { createSupabaseBrowserClient } from "@/lib/supabase/client";
import { Button } from "@/components/ui/button";
import Header from "@/components/header";
import { useEffect, useState } from "react";
import { Card } from "@/components/ui/card";
import { getIcon } from "@/lib/icons";
import Link from "next/link";

export default function Profile({ params }: { params: { slug: string } }) {
  const supabase = createSupabaseBrowserClient();
  const [profile, setProfile] = useState<any>();
  const [profileImageUrl, setProfileImageUrl] = useState<string>();

  useEffect(() => {
    const fetchProfile = async () => {
      const { data } = await supabase
        .from("profiles")
        .select("*")
        .eq("user_id", params?.slug);
      setProfile(data);
    };

    fetchProfile();
  }, []);

  useEffect(() => {
    if (profile) {
      const { data } = supabase.storage
        .from("profile-images")
        .getPublicUrl(`${profile?.[0]?.user_id}/profile?t=${Date.now()}`);

      if (data?.publicUrl) {
        setProfileImageUrl(data?.publicUrl);
      }
    }
  }, [profile]);

  return (
    <>
      <Header profile={profile} setProfile={setProfile} />
      <div className="flex flex-col mx-4 mt-20 my-4 gap-10">
        <Card className="flex w-full h-[200px]">
          <div className="w-2/5 flex items-center justify-center border border-dotted border-r-1 border-t-0 border-b-0 border-l-0">
            {profileImageUrl && (
              <img
                src={profileImageUrl}
                alt="Profile Image"
                className="w-full h-full object-cover"
              />
            )}
          </div>
          <div className="w-3/5 flex items-center px-4">
            <div className="flex flex-col">
              <h1 className="scroll-m-20 text-2xl font-extrabold tracking-tight">
                {profile?.[0]?.name}
              </h1>
              <p className="text-md text-muted-foreground">
                {profile?.[0]?.title}
              </p>
            </div>
          </div>
        </Card>
        <div className="flex flex-col w-full gap-4">
          <h3 className="scroll-m-20 text-2xl font-semibold tracking-tight">
            About
          </h3>
          <p className="text-md text-muted-foreground">{profile?.[0]?.bio}</p>
        </div>
        <div className="flex flex-col w-full gap-4">
          <h3 className="scroll-m-20 text-2xl font-semibold tracking-tight">
            Social Network
          </h3>
          <div className="grid grid-cols-5 gap-6">
            {profile?.[0]?.social_media_url?.map((url: any, index: number) => {
              return (
                <Link
                  key={index}
                  href={url?.url}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <Button variant="outline" className="rounded-lg w-14 h-14">
                    {getIcon(url?.app)}
                  </Button>
                </Link>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}
