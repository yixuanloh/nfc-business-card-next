import {
  FaFacebook,
  FaWhatsapp,
  FaInstagram,
  FaLinkedin,
} from "react-icons/fa";
import { FaXTwitter } from "react-icons/fa6";
import { LuMail } from "react-icons/lu";

export const icons = [
  {
    label: "Facebook",
    value: "facebook",
    icon: <FaFacebook className="h-6 w-6" />,
  },
  {
    label: "Linkedin",
    value: "linkedin",
    icon: <FaLinkedin className="h-6 w-6" />,
  },
  {
    label: "Twitter",
    value: "twitter",
    icon: <FaXTwitter className="h-6 w-6" />,
  },
  {
    label: "Whatsapp",
    value: "whatsapp",
    icon: <FaWhatsapp className="h-6 w-6" />,
  },
  {
    label: "Email",
    value: "email",
    icon: <LuMail className="h-6 w-6" />,
  },
  {
    label: "Instagram",
    value: "instagram",
    icon: <FaInstagram className="h-6 w-6" />,
  },
];

export const getIcon = (app: string) => {
  return icons.find((icon) => icon.value === app)?.icon;
};