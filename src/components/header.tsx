"use client";
import { Button } from "@/components/ui/button";
import { createSupabaseBrowserClient } from "@/lib/supabase/client";
import { LuFileEdit, LuLogOut, LuShare2 } from "react-icons/lu";
import { useRouter, usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import {
  Dialog,
  DialogContent,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import {
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerTitle,
  DrawerTrigger,
} from "@/components/ui/drawer";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Input } from "@/components/ui/input";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { useToast } from "@/components/ui/use-toast";
import { useFieldArray, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import { getIcon } from "@/lib/icons";
import { TiDelete } from "react-icons/ti";
import { LuPlus, LuLoader } from "react-icons/lu";
import { icons } from "@/lib/icons";

type HeaderProps = {
  profile: any;
  setProfile: any;
};

const socialNetworkSchema = z.object({
  app: z.string(),
  url: z.string(),
});

const schema = z.object({
  name: z.string(),
  title: z.string(),
  bio: z.string(),
  social_media_url: z.array(socialNetworkSchema),
});

type ProfileSchemaType = z.infer<typeof schema>;

export default function Header({ profile, setProfile }: HeaderProps) {
  const router = useRouter();
  const { toast } = useToast();
  const pathname = usePathname();
  const supabase = createSupabaseBrowserClient();
  const [session, setSession] = useState<any>();
  const [isDialogOpen, setDialogOpen] = useState<boolean>(false);
  const [isDrawerOpen, setDrawerOpen] = useState<boolean>(false);
  const [socialNetwork, setSocialNetwork] = useState<{
    app: string;
    url: string;
  }>({ app: "", url: "" });
  const [profileImage, setProfileImage] = useState<File | null>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const methods = useForm<ProfileSchemaType>({
    resolver: zodResolver(schema),
    defaultValues: {
      name: "",
      title: "",
      bio: "",
      social_media_url: [],
    },
  });

  const { control, handleSubmit } = methods;

  const { fields, append, remove } = useFieldArray({
    control,
    name: "social_media_url",
  });

  useEffect(() => {
    methods.reset({
      name: profile?.[0]?.name || "",
      title: profile?.[0]?.title || "",
      bio: profile?.[0]?.bio || "",
      social_media_url: profile?.[0]?.social_media_url || [],
    });
    setProfileImage(null);
  }, [profile, isDialogOpen]);

  useEffect(() => {
    if (!isDrawerOpen) {
      setSocialNetwork({ app: "", url: "" });
    }
  }, [isDrawerOpen]);

  const onSubmit = async (values: ProfileSchemaType) => {
    setIsLoading(true);
    if (profileImage) {
      const { data: uploadData, error: uploadError } = await supabase.storage
        .from("profile-images")
        .upload(profile?.[0]?.user_id + "/profile", profileImage, {
          upsert: true,
        });
    }

    const socialMediaUrl = values?.social_media_url?.map((media) =>
      media.app === "email"
        ? { app: media.app, url: `mailto:${media.url}` }
        : media
    );

    const { error } = await supabase
      .from("profiles")
      .update({ ...values, social_media_url: socialMediaUrl })
      .eq("user_id", profile?.[0]?.user_id);

    if (error) {
      toast({
        variant: "destructive",
        description: error.message,
      });
    } else {
      const { data, error } = await supabase
        .from("profiles")
        .select("*")
        .eq("user_id", profile?.[0]?.user_id);

      if (error) {
        toast({
          variant: "destructive",
          description: error.message,
        });
      } else {
        setProfile(data);
        setDialogOpen(false);
      }
    }
    setIsLoading(false);
  };

  useEffect(() => {
    const fetchSession = async () => {
      const {
        data: { session },
      } = await supabase.auth.getSession();
      setSession(session);
    };

    fetchSession();
  }, []);

  const handleLogout = async () => {
    await supabase.auth.signOut();
    router.push("/");
  };

  const handleShare = () => {
    if (navigator.share) {
      navigator.share({
        url: pathname,
      });
    }
  };

  return (
    <div className="fixed top-0 w-full flex h-14 px-4 items-center justify-end space-x-4 border-b border-border/40 bg-background/95 backdrop-blur supports-[backdrop-filter]:bg-background/60">
      <Button variant="outline" size="icon" onClick={handleShare}>
        <LuShare2 className="h-5 w-4" />
      </Button>
      {session && (
        <>
          <Dialog open={isDialogOpen} onOpenChange={setDialogOpen}>
            <DialogTrigger asChild>
              <Button variant="outline" size="icon">
                <LuFileEdit className="h-5 w-4" />
              </Button>
            </DialogTrigger>
            <DialogContent className="w-11/12 sm:max-w-md rounded-lg">
              <DialogHeader>
                <DialogTitle>Edit profile</DialogTitle>
              </DialogHeader>
              <Form {...methods}>
                <form
                  onSubmit={handleSubmit(onSubmit)}
                  className="w-full space-y-4"
                >
                  <FormItem>
                    <FormLabel>Profile Image</FormLabel>
                    <FormControl>
                      <Input
                        accept="image/*"
                        type="file"
                        id="upload-image"
                        placeholder="Your image"
                        className="text-base"
                        onChange={(event) => {
                          if (event.target.files) {
                            setProfileImage(event.target.files[0]);
                          }
                        }}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                  <FormField
                    control={control}
                    name="name"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Name</FormLabel>
                        <FormControl>
                          <Input
                            placeholder="Your name"
                            {...field}
                            className="text-base"
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={control}
                    name="title"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Title</FormLabel>
                        <FormControl>
                          <Input
                            placeholder="Your title"
                            {...field}
                            className="text-base"
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <FormField
                    control={control}
                    name="bio"
                    render={({ field }) => (
                      <FormItem>
                        <FormLabel>Bio</FormLabel>
                        <FormControl>
                          <Input
                            placeholder="Your bio"
                            {...field}
                            className="text-base"
                          />
                        </FormControl>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                  <div className="flex flex-col space-y-4">
                    <FormLabel>Social Network</FormLabel>
                    <div className="grid grid-cols-5 gap-6">
                      {fields?.map((icon, index) => (
                        <Button
                          key={index}
                          variant="outline"
                          className="rounded-lg w-14 h-14 relative"
                          type="button"
                        >
                          {getIcon(icon?.app)}
                          <div
                            className="absolute top-0 right-0 translate-x-1/2 -translate-y-1/2 cursor-pointer"
                            onClick={() => remove(index)}
                          >
                            <TiDelete className="h-6 w-6 text-destructive" />
                          </div>
                        </Button>
                      ))}
                      <Drawer open={isDrawerOpen} onOpenChange={setDrawerOpen}>
                        <DrawerTrigger asChild>
                          <Button
                            variant="outline"
                            className="rounded-lg w-14 h-14 relative"
                            type="button"
                          >
                            <LuPlus className="h-6 w-6" />
                          </Button>
                        </DrawerTrigger>
                        <DrawerContent>
                          <div className="mx-auto w-full max-w-sm">
                            <DrawerHeader>
                              <DrawerTitle>Select Social Network</DrawerTitle>
                            </DrawerHeader>
                            <div className="py-4 px-16">
                              <div className="flex flex-col items-center justify-center gap-4">
                                <Select
                                  onValueChange={(value) =>
                                    setSocialNetwork((prev) => ({
                                      ...prev,
                                      app: value,
                                    }))
                                  }
                                >
                                  <SelectTrigger>
                                    <SelectValue placeholder="Select social network" />
                                  </SelectTrigger>
                                  <SelectContent>
                                    <SelectGroup>
                                      {icons.map((icon, index) => (
                                        <SelectItem
                                          key={index}
                                          value={icon.value}
                                        >
                                          {icon.label}
                                        </SelectItem>
                                      ))}
                                    </SelectGroup>
                                  </SelectContent>
                                </Select>
                                <Input
                                  placeholder="Url"
                                  onChange={(e) =>
                                    setSocialNetwork((prev) => ({
                                      ...prev,
                                      url: e.target.value,
                                    }))
                                  }
                                  className="text-base"
                                />
                              </div>
                            </div>
                            <DrawerFooter>
                              <Button
                                variant="default"
                                type="button"
                                className="w-full"
                                onClick={() => {
                                  const isExists = fields?.find(
                                    (social) => social.app === socialNetwork.app
                                  );
                                  if (
                                    !socialNetwork.app ||
                                    !socialNetwork.url
                                  ) {
                                    toast({
                                      variant: "destructive",
                                      description: "Fields cannot be empty",
                                    });
                                  } else if (isExists) {
                                    toast({
                                      variant: "destructive",
                                      description: "Duplicate app",
                                    });
                                  } else {
                                    append(socialNetwork);
                                    setSocialNetwork({ app: "", url: "" });
                                    setDrawerOpen(false);
                                  }
                                }}
                              >
                                Save Changes
                              </Button>
                            </DrawerFooter>
                          </div>
                        </DrawerContent>
                      </Drawer>
                    </div>
                  </div>
                  <DialogFooter>
                    <Button type="submit" className="w-full">
                      {isLoading ? (
                        <LuLoader className="animate-spin" size={24} />
                      ) : (
                        "Save Changes"
                      )}
                    </Button>
                  </DialogFooter>
                </form>
              </Form>
            </DialogContent>
          </Dialog>
          <Button variant="outline" size="icon" onClick={handleLogout}>
            <LuLogOut className="h-5 w-4" />
          </Button>
        </>
      )}
    </div>
  );
}
