"use client";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { createSupabaseBrowserClient } from "@/lib/supabase/client";
import { Auth } from "@supabase/auth-ui-react";
import { ThemeSupa } from "@supabase/auth-ui-shared";

export default function SignIn() {
  const supabase = createSupabaseBrowserClient();

  return (
    <Card className="w-[350px]">
      <CardHeader>
        <CardTitle>Shortcut Asia</CardTitle>
      </CardHeader>
      <CardContent>
        <Auth
          supabaseClient={supabase}
          appearance={{
            theme: ThemeSupa,
            variables: {
              default: {
                colors: {
                  defaultButtonBackground: '#22c55e',
                  defaultButtonBackgroundHover: '#22c55e',
                  defaultButtonBorder: '#22c55e',
                  defaultButtonText: 'black',
                },
              },
            },
          }}
          providers={["google"]}
          onlyThirdPartyProviders
          redirectTo={process.env.NEXT_PUBLIC_AUTH_CALLBACK_URL!}
        />
      </CardContent>
    </Card>
  );
}
